FROM ghcr.io/void-linux/void-linux:latest-mini-bb-x86_64 as builder
RUN xbps-install -Sy cargo gcc
RUN cargo install mdbook-mermaid
RUN cargo install --git https://gitlab.com/famedly/tools/mdbook-aggregator.git --branch main
FROM ghcr.io/void-linux/void-linux:latest-mini-bb-x86_64
COPY --from=builder /root/.cargo/bin/mdbook-mermaid /usr/local/bin/mdbook-mermaid
COPY --from=builder /root/.cargo/bin/mdbook-aggregator /usr/local/bin/mdbook-aggregator
RUN xbps-install -Sy mdBook mdbook-linkcheck mdbook-toc
